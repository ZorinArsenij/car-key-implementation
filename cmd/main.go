package main

import (
	"crypto/ecdsa"
	"crypto/elliptic"
	"crypto/rand"
	"log"

	"gitlab.com/ZorinArsenij/car-key-implementation/internal"
)

func main() {
	carPrivateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatalf("failed to generate car private key: %s", err)
	}

	trinketPrivateKey, err := ecdsa.GenerateKey(elliptic.P256(), rand.Reader)
	if err != nil {
		log.Fatalf("failed to generate trinket private key: %s", err)
	}

	log.Printf("(registration) X=%d Y=%d (pubkey1 written to trinked), X=%d Y=%d (pubkey2 written to car)",
		trinketPrivateKey.PublicKey.X, trinketPrivateKey.PublicKey.Y,
		carPrivateKey.PublicKey.X, carPrivateKey.PublicKey.Y)

	car := internal.NewCar(*carPrivateKey, trinketPrivateKey.PublicKey)
	trinket := internal.NewTrinket(*trinketPrivateKey, carPrivateKey.PublicKey)

	requestToCar := trinket.GenerateRequestToCar(internal.Open)
	log.Printf("(handshake) trinket -> car, %s (command), %x (challenge for car)",
		requestToCar.Data.Command, requestToCar.Data.ChallengeRequest.Data)

	responseToTrinket, err := car.ProcessRequestFromTrinket(requestToCar)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("(challenge) car -> trinket: %x (challenge for trinket), %x (confirm challenge for car)",
		responseToTrinket.Data.ChallengeRequest.Data, responseToTrinket.Data.ChallengeResponse.Data)

	responseToCar, err := trinket.ProcessResponseFormCar(responseToTrinket)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("(response) trinket -> car: %x (confirm challenge for trinket)",
		responseToCar.Data.ChallengeResponse.Data)

	if err := car.ProcessResponseFromTicker(responseToCar); err != nil {
		log.Fatal(err)
	}

	log.Printf("(action) car: check response - ok, %s",
		responseToCar.Data.Command)
}
