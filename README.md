# Car key implementation

Command line program which realize protocol of communication between car and remote key

# Build
Install Golang:
```bash
curl -O https://storage.googleapis.com/golang/go1.13.5.linux-amd64.tar.gz
tar -xvf go1.13.5.linux-amd64.tar.gz
sudo mv go /usr/local
```
Build code:
```bash
go build -o program cmd/main.go
```

# Run
Example of command for running program:
```bash
./program
```

Example of output:
```
(registration) X=86305646560166784339936035404868178369613176556288611881349083875293048906196 Y=39955705245264253061619940636848065031759989395727056809628549640162589076929 (pubkey1 written to trinked), X=83967660718641841483840626631444198215610361859911747518010656901550429221328 Y=106601096889359077428231929203470641106279370708714344545042561515857085798960 (pubkey2 written to car)
(handshake) trinket -> car, open (command), 4598865f00000000cfa037f561620854311f6721c75a111642acd6b0cd522d0c1a0ae6bf10ef29be (challenge for car)
(challenge) car -> trinket: 4598865f00000000a09f558113573ffbbf6b075fb22ff774c7b930dd5308b11975b1158be22c7d20 (challenge for trinket), 4598865f00000000cfa037f561620854311f6721c75a111642acd6b0cd522d0c1a0ae6bf10ef29be (confirm challenge for car)
(response) trinket -> car: 4598865f00000000a09f558113573ffbbf6b075fb22ff774c7b930dd5308b11975b1158be22c7d20 (confirm challenge for trinket)
(action) car: check response - ok, open
```

# Protocol
1. trinket (data: challenge for car) -> car
2. car (data: challenge for trinket and solved challenge for car) -> trinket (validate solved challenge for car)
3. trinket (data: solved challenge for trinket) -> car (validate solved challenge for trinket) (on success perform command)

All transferred data between trinket and car is signed so that it cannot be changed by the third side
