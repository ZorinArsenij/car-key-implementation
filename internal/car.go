package internal

import (
	"crypto/ecdsa"
	"fmt"
)

type Car struct {
	opened bool

	PrivateKey       ecdsa.PrivateKey
	TrinketPublicKey ecdsa.PublicKey

	CurrentChallenge Challenge
}

func NewCar(privateKey ecdsa.PrivateKey, trinketPublicKey ecdsa.PublicKey) Car {
	return Car{
		PrivateKey:       privateKey,
		TrinketPublicKey: trinketPublicKey,
	}
}

func (c *Car) ProcessRequestFromTrinket(request Request) (Request, error) {
	// Verify sig of request data
	if !request.Verify(c.TrinketPublicKey) {
		return Request{}, fmt.Errorf("invalid sig for request data")
	}

	// Create challenge for trinket
	c.CurrentChallenge = NewChallenge()

	data := Data{
		Command:           request.Data.Command,
		ChallengeRequest:  c.CurrentChallenge,
		ChallengeResponse: request.Data.ChallengeRequest,
	}

	// Generate sig of challenge received from trinket
	challengeSig := GenerateSig(request.Data.ChallengeRequest.Data, c.PrivateKey)
	data.ChallengeResponse.Sig = &challengeSig

	// Make request with data of solved challenge received from trinket, challenge for trinket and sig of data
	return NewRequest(data, c.PrivateKey), nil
}

func (c *Car) ProcessResponseFromTicker(request Request) error {
	// Verify sig of request data
	if !request.Verify(c.TrinketPublicKey) {
		return fmt.Errorf("invalid sig for request data")
	}

	// Verify sig of challenge for trinket
	if !VerifySig(c.CurrentChallenge.Data, c.TrinketPublicKey, *request.Data.ChallengeResponse.Sig) {
		return fmt.Errorf("invalid challenge sig")
	}

	c.CurrentChallenge.Data = nil

	switch request.Data.Command {
	case Open:
		c.opened = true
	case Close:
		c.opened = false
	}

	return nil
}

func (c Car) IsOpened() bool {
	return c.opened
}
