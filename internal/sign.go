package internal

import (
	"crypto/ecdsa"
	"crypto/rand"
)

func GenerateSig(data []byte, privateKey ecdsa.PrivateKey) Sig {
	r, s, _ := ecdsa.Sign(rand.Reader, &privateKey, data)

	return Sig{
		R: *r,
		S: *s,
	}
}

func VerifySig(data []byte, publicKey ecdsa.PublicKey, sig Sig) bool {
	return ecdsa.Verify(&publicKey, data, &sig.R, &sig.S)
}
