package internal

import (
	"crypto/ecdsa"
	"fmt"
)

const (
	Open  = "open"
	Close = "close"
)

type Trinket struct {
	PrivateKey   ecdsa.PrivateKey
	CarPublicKey ecdsa.PublicKey

	CurrentChallenge Challenge
}

func NewTrinket(privateKey ecdsa.PrivateKey, carPublicKey ecdsa.PublicKey) Trinket {
	return Trinket{
		PrivateKey:   privateKey,
		CarPublicKey: carPublicKey,
	}
}

func (t *Trinket) GenerateRequestToCar(command string) Request {
	// Create challenge for car
	t.CurrentChallenge = NewChallenge()

	data := Data{
		Command:          command,
		ChallengeRequest: t.CurrentChallenge,
	}

	// Make request with challenge for car in data and sig of data
	return NewRequest(data, t.PrivateKey)
}

func (t *Trinket) ProcessResponseFormCar(request Request) (Request, error) {
	// Verify sig of request data
	if !request.Verify(t.CarPublicKey) {
		return Request{}, fmt.Errorf("invalid sig for request data")
	}

	// Verify sig of challenge for car
	if !VerifySig(t.CurrentChallenge.Data, t.CarPublicKey, *request.Data.ChallengeResponse.Sig) {
		return Request{}, fmt.Errorf("invalid challenge sig")
	}

	data := Data{
		Command:           request.Data.Command,
		ChallengeResponse: request.Data.ChallengeRequest,
	}

	// Generate sig of challenge received from car
	challengeSig := GenerateSig(request.Data.ChallengeRequest.Data, t.PrivateKey)
	data.ChallengeResponse.Sig = &challengeSig

	t.CurrentChallenge.Data = nil

	// Make request with data of solved challenge received from car and sig of data
	return NewRequest(data, t.PrivateKey), nil
}
