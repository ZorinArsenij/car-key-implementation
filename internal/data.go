package internal

import (
	"crypto/ecdsa"
	"crypto/rand"
	"crypto/sha256"
	"encoding/binary"
	"encoding/json"
	"math/big"
	"time"
)

type Request struct {
	Data Data
	Sig  Sig // sig of request
}

// NewRequest create request with signed data
func NewRequest(data Data, privateKey ecdsa.PrivateKey) Request {
	d, _ := json.Marshal(data)
	sig := GenerateSig(d, privateKey)

	return Request{
		Data: data,
		Sig:  sig,
	}
}

func (r Request) Verify(publicKey ecdsa.PublicKey) bool {
	d, _ := json.Marshal(r.Data)
	return VerifySig(d, publicKey, r.Sig)
}

func (r *Request) SigData(privateKey ecdsa.PrivateKey) {
	d, _ := json.Marshal(r.Data)
	r.Sig = GenerateSig(d, privateKey)
}

type Challenge struct {
	Data []byte
	Sig  *Sig
}

func NewChallenge() Challenge {
	b := make([]byte, 256)
	rand.Read(b)

	hash := sha256.New()
	hash.Write(b)

	timestamp := make([]byte, 8)
	binary.LittleEndian.PutUint64(timestamp, uint64(time.Now().Unix()))

	return Challenge{
		Data: hash.Sum(timestamp),
	}
}

type Sig struct {
	R big.Int
	S big.Int
}

type Data struct {
	Command           string
	ChallengeRequest  Challenge
	ChallengeResponse Challenge
}
